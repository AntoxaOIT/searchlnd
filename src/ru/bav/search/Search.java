package ru.bav.search;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс для поиска идентификатора из файла с расширением .java
 *
 * @author Barinov group 15OIT18.
 */



public class Search {
    public static void main(String[] args) throws IOException {
        String string;
        Pattern p = Pattern.compile(".+"); //шаблон для записи всей программы.
        Matcher matcherCode = p.matcher("");

        Pattern spaces = Pattern.compile("\\s+"); // шаблон для поиска пробелов
        Matcher matcherSpaces = spaces.matcher("");

        Pattern pattern = Pattern.compile("[A-Za-z_]+\\w*"); //для идентификаторов
        Matcher matcherIdn = pattern.matcher("");

        Pattern pComment = Pattern.compile("\\/\\*\\*.+?\\*\\/"); //для комментариев
        Matcher matcherComm = pComment.matcher("");


        try (
                BufferedReader reader = new BufferedReader(new FileReader("Calculator.java"));
                BufferedWriter writer = new BufferedWriter(new FileWriter("Code1String.txt"))
        ) {
            while ((string = reader.readLine()) != null) {
                matcherCode.reset(string);

                while (matcherCode.find()) {
                    writer.write((matcherCode.group()));
                }
            }
        }


        try (BufferedWriter writer = new BufferedWriter(new FileWriter("NoComm.txt")); //Запись кода без комментариев.
             BufferedReader Code1String = new BufferedReader(new FileReader("Code1String.txt"))) //Поиск в файле Code1String.txt комментариев
        {
            while ((string = Code1String.readLine()) != null) {
                matcherComm.reset(string); //подключение строки к matcher
                while (matcherComm.find()) {
                    string = string.replaceAll("\\/\\*\\*.+?\\*\\/", " ");
                }
                matcherCode.reset(string);
                while (matcherCode.find()) {
                    writer.write(matcherCode.group());
                }
            }
        }

        try (
                BufferedWriter writer = new BufferedWriter(new FileWriter("IdnWithoutComm.txt"));
                BufferedReader CodeString = new BufferedReader(new FileReader("NoComm.txt"))
        ) {
            while ((string = CodeString.readLine()) != null) {
                matcherIdn.reset(string);
                while (matcherIdn.find()) {
                    writer.write(matcherIdn.group() + "\n"); //Запись идентификаторов без комментариев.
                }
            }
        }



        try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File("NoCommANDSpaces.java")));
             BufferedReader code = new BufferedReader(new FileReader("NoComm.txt"))) {
            while ((string = code.readLine()) != null) {
                matcherSpaces.reset(string);
                while (matcherSpaces.find()) {
                    string = string.replaceAll("\\s+", " ");
                }
                matcherCode.reset(string);
                while (matcherCode.find()) {
                    writer.write(matcherCode.group()); // запись без лишних пробельных символов
                }

            }
        }

    }
}

