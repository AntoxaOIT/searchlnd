В проекте реализовано :

Чтение файла и поиск всех идентификаторов, кроме тех что находятся в комментариях.


Запись найденных в исходном файле идентификаторов в текстовый файл по одному в каждой строке.

Поиск в исходном файле комментариев и замена их на пустую строку.

Запись исходного файла без комментариев в текстовый файл.

Удаление лишних пробельных символов и комментариев из исходного файла и запись результата в текстовый файл.